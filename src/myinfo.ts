import express from "express";
import request from "request";
import { data_file, discord_client } from "./index";
import * as files from "./file_handling";

const app = express.Router();

app.get("/", (req, res) => {
    let id = req.cookies.id;
    let data = files.readFile(data_file);
    // handle errors
    if (!id || !(data && data[id])) {
        res.json({
            error: "Invalid ID."
        });
        return;
    }
    if (!data) {
        res.json({
            error: "Server error."
        })
        return;
    }
    // prepare request options
    const options: request.UriOptions & request.CoreOptions = {
        uri: "https://discordapp.com/api/users/@me",
        auth: {
            bearer: data[id]["access_token"]
        },
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "User-Agent": "Mozilla/5.0",
            "Accept": "*/*"
        }
    };
    // send request to options.uri
    request(options, (error, response, body) => {
        let json_body = JSON.parse(body);
        if (error || (json_body && json_body["error"])) {
            res.json({
                error: "Failed to contact Discord."
            });
            return;
        }
        // is valid data?
        if (!json_body["username"]) {
            // it isn't, so generate new access token
            const options: request.UriOptions & request.CoreOptions = {
                uri: "https://discordapp.com/api/oauth2/token",
                method: "POST",
                form: {
                    client_id: discord_client.id,
                    client_secret: discord_client.secret,
                    grant_type: "refresh_token",
                    refresh_token: data[id]["refresh_token"],
                    redirect_uri: "http://localhost:1337/callback",
                    scope: "identify email"
                },
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "User-Agent": "Mozilla/5.0",
                    "Accept": "*/*"
                }
            };
            // send request to get new token
            request(options, (error, response, body) => {
                if (error) {
                    res.json({
                        error: "Failed to refresh Discord token."
                    });
                    return;
                }
                let body_json = JSON.parse(body);
                if (!body_json || body_json["error"]) {
                    return;
                }
                data[id] = {
                    access_token: body_json["access_token"],
                    refresh_token: body_json["refresh_token"]
                };
                // save the data, and refresh the page
                files.writeFile(data_file, JSON.stringify(data, null, 2));
                res.redirect(req.baseUrl);
            });
            return;
        }
        // send back person's discord information
        res.json(json_body);
    });
});

export = app;
