import express from "express";
import request from "request";
import cookieParser from "cookie-parser";
import * as path from "path";
import * as crypto from "crypto";
import * as files from "./file_handling";

export const data_file: string = "data.json";
let data = files.readFile(data_file);
if (data === null) {
    // the defaults
    data = {};
    files.writeFile(data_file, JSON.stringify(data, null, 2));
}

// holds the client's credentials
export const discord_client = {
    id: "589481333152022528", // DiscordGet
    secret: "SOME SECRET HERE"
};

// the app
const app = express();
const getTemplatePath = () => path.join(__dirname, "..", "templates"); // method to get the template path
const generateRandomString = () => crypto.randomBytes(16).toString("hex"); // method to generate a random ID

// the port
const port: number = 1337;

app.get("/callback", (req, res) => {
    // fail page getter
    let getFailPage = () => path.join(getTemplatePath(), "callback", "fail.html");

    // the code parameter set by discord (URL?code=x)
    const code_str: string | undefined = req.query.code;
    if (!code_str) {
        res.sendFile(getFailPage());
        return;
    }
    // the request data | options
    const options: request.UriOptions & request.CoreOptions = {
        uri: "https://discordapp.com/api/oauth2/token",
        method: "POST",
        form: {
            client_id: discord_client.id,
            client_secret: discord_client.secret,
            grant_type: "authorization_code",
            code: code_str,
            redirect_uri: "http://localhost:1337/callback",
            scope: "identify email"
        },
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "User-Agent": "Mozilla/5.0",
            "Accept": "*/*"
        }
    };
    // send request to options.uri
    request(options, (error, response, body) => {
        // handle errors (send error page)
        if (error || !body) {
            res.sendFile(getFailPage());
            return;
        }
        // parse the body as a json object
        const json_body = JSON.parse(body);
        if (json_body["error"]) {
            res.sendFile(getFailPage());
            return;
        }

        // save the access and refresh token
        let random_id = generateRandomString();
        while (data[random_id]) { // prevent duplicates (assuming that may ever happen)
            random_id = generateRandomString();
        }
        // store the user's id in their cookies for easy identification
        res.cookie("id", random_id);
        data[random_id] = {
            access_token: json_body["access_token"],
            refresh_token: json_body["refresh_token"]
        };
        files.writeFile(data_file, JSON.stringify(data, null, 2));

        // success
        res.sendFile(path.join(getTemplatePath(), "callback", "success.html"));
    });
});

// deny access to this directory because it shouldn't be directly accessed
app.all("/callback/*", (req, res) => {
    res.status(403).send("Forbidden access.");
});

// cookie parser (makes req.cookies available)
app.use(cookieParser());

// use the template path, along with css and assets
app.use(express.static(getTemplatePath()));
app.use(express.static(path.join(getTemplatePath(), "css")));
app.use(express.static(path.join(getTemplatePath(), "assets")));

// allows users to get their discord info
app.use("/api/myinfo", require("./myinfo"));

// listen on the ${port}
app.listen(port, () => console.log(`Server created, listening on port ${port}`));
