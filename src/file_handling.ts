import * as fs from "fs";

export function readFile(file: string, parse_json: boolean = true) {
    if (!fs.existsSync(file)) {
        return null;
    }
    const read = fs.readFileSync(file, "utf-8");
    if (!parse_json) {
        return read;
    }
    return JSON.parse(read);
};

export function writeFile(file: string, value: string) {
    fs.writeFileSync(file, value, "utf-8");
};
